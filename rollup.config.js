import babel from 'rollup-plugin-babel';
import builtins from 'rollup-plugin-node-builtins';
import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import globals from 'rollup-plugin-node-globals';
import commonjs from 'rollup-plugin-commonjs';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import md from 'svelte-preprocess-md';

const production = !process.env.ROLLUP_WATCH;

export default {
	input: 'src/main.js',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: 'admin/bundle.js'
	},
	plugins: [
		svelte({
      // allow us to use markdown in our scripts
      extensions: ['.svelte'],
      preprocess: md(),
			// enable run-time checks when not in production
			dev: !production,
			// we'll extract any component CSS out into
			// a separate file — better for performance
			css: css => {
				css.write('admin/bundle.css');
			}
		}),
		commonjs(),
    builtins(),
    globals(),
		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration —
		// consult the documentation for details:
		// https://github.com/rollup/rollup-plugin-commonjs
		resolve({
      preferBuiltins: false,
			browser: true,
			dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/')
		}),
			babel({
				extensions: ['.js', '.mjs', '.html', '.svelte'],
				runtimeHelpers: true,
				exclude: ['node_modules/@babel/**'],
				presets: [
					['@babel/preset-env', {
						targets: '> 0.25%, not dead'
					}]
				],
				plugins: [
					'@babel/plugin-syntax-dynamic-import',
					['@babel/plugin-transform-runtime', {
						useESModules: true
					}]
				]
			}),

		// Watch the `admin` directory and refresh the
		// browser on changes when not in production
		!production && livereload('admin'),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser()
	],
	watch: {
		clearScreen: false
	},
  onwarn: (warning, warn) => {
    // skip certain warnings
    if (warning.code === 'CIRCULAR DEPENDENCY') return;
    // throw on others
    if (warning.code === 'NON_EXISTENT_EXPORT') throw new Error(warning.message);
    // Use default for everything else
    warn(warning);
  }
};

