import { writable, derived } from 'svelte/store';
import { newSongIsland } from './new-songIsland.js';
import rgbhex from 'rgb-hex';
import hexrgb from 'hex-rgb'

//given an RGB object, and a chosen opacity, return an rgba string that's CSS ready.
const createRGBA = (rgb, opacity) => `rgba(${rgb.red}, ${rgb.green}, ${rgb.blue}, ${opacity})`

// rgba colors with less opacity don't play
// well with our hex converter, so we
// make them into a nicer rgb, removing
// the last value.
function RGBAToRGB (rgba) {
  let vals = rgba.replace(/(rgba\()|(\))/g,'').split(',')
  return `rgb(${vals[0]}, ${ vals[1] }, ${ vals[2] })`
}

export const aesthetic = writable({
  accentColor: '',
  backgroundColor: '',
  picture: '',
  backgroundPosition: '',
  backgroundSize: '',
  color: '',
  gradient: {
    top: '#000000',
    bottom: '#ffffff',
    opacity: '0.5'
  },
  primaryColor: '',
  theme: 'cats'
})

export const gradientRGBs = derived(
  aesthetic,
  ($aesthetic, set) => {
    set({
      top: hexrgb($aesthetic.gradient.top),
      bottom: hexrgb($aesthetic.gradient.bottom),
      opacity: $aesthetic.gradient.opacity
    })
  }
)

export const gradient = derived(
  gradientRGBs,
  ($g, set) => {
    let top = createRGBA($g.top, $g.opacity)
    let bottom = createRGBA($g.bottom, $g.opacity)
    let opacity = $g.opacity
    set({
      top,
      bottom,
      opacity,
      display: `linear-gradient(${top}, ${bottom})`
    })
  }
)

document.addEventListener('DOMContentLoaded', () => {
  // We'll check the current style of the page, and use that to
  // fill out the initial look in the edit.  We need to convert stuff
  // to hex, as that's the only thing the color input form will take.
  const bodyStyle = window.getComputedStyle(document.body, null);
  const backgroundColor = "#" + rgbhex(bodyStyle.backgroundColor);
  const gradientStyle = window.getComputedStyle(document.querySelector('#aesthetic-hack.gradient'));
  const mainStyle = window.getComputedStyle(document.querySelector('#aesthetic-hack.main'));
  const color = "#" + rgbhex(bodyStyle.color);
  let gradTop = RGBAToRGB(gradientStyle.backgroundColor);
  let gradBottom = RGBAToRGB(gradientStyle.color);
  let gradOpacity = gradientStyle.opacity;
  let primaryColor = '#' + rgbhex(mainStyle.backgroundColor);
  let accentColor = '#' + rgbhex(mainStyle.color);
  let theme = mainStyle.content.replace(/"/g,'');

  const gradient = {
    top: '#' + rgbhex(gradTop),
    bottom: '#' + rgbhex(gradBottom),
    opacity: gradOpacity
  };

  aesthetic.set({
    backgroundColor,
    picture: bodyStyle.backgroundImage,
    primaryColor: primaryColor,
    accentColor: accentColor,
    backgroundSize: bodyStyle.backgroundSize,
    backgroundPosition: bodyStyle.backgroundPosition,
    backgroundRepeat: bodyStyle.backgroundRepeat,
    color,
    gradient,
    theme
  })
})
