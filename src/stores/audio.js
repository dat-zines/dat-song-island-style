import { writable, derived } from 'svelte/store';

// the audio element will be created By App.svelte on load using the <audio> element.
let audioSrc;

export const audio = writable(
  {
    time: 0,
    duration: 0,
    paused: true,
    audio: audioSrc
  });

//ct is the current time made READABLE
// dt is duration, also made readable.
// I didn't know the best way to give them unique names
// that weren't hella long like 'currentTimeFormmatedForReadability'
// so i just made this comment, that's four times as long as any
// variable i could've chose.
//
// 'the foolish ones are those that think too much.'
// - Dave Matthews, "Crash"
export const ct = derived(audio, $a => format($a.time));
export const dt = derived(audio, $a => format($a.duration));

function format(seconds) {
  if (isNaN(seconds)) return '...';

  const minutes = Math.floor(seconds / 60);
  seconds = Math.floor(seconds % 60);
  if (seconds < 10) seconds = '0' + seconds;

  return `${minutes}:${seconds}`;
}
