import { readable } from 'svelte/store';
import { parse } from 'smarkt';

export const distro = readable({}, function start(set) {
  fetch('distro/info.zine')
    .then(response => response.text())
    .then(text => set(parse(text)))

  return function stop () {
    console.log('no more songs');
  }
});
