import { writable, derived } from 'svelte/store';
import { parse } from 'smarkt';

let today = new Date()
    .toISOString()
    .split("T")[0];

export const newDistro = writable({
  "creator": '',
  "creator homepage": '',
  "creation date": today,
  "sharing": '',
  "riyl": [],
  "moods": []
})

export const riyl = derived(
  newDistro,
  ($newDistro, set) => set($newDistro.riyl)
);

document.addEventListener('DOMContentLoaded', () => {
  fetch('/distro/info.zine')
    .then(response => response.text())
    .then(text => parse(text))
    .then(parsed => newDistro.set({...parsed, "creation date": today}))
})
