//  This is the store to hold people's selections within the edit page
//  When they save their edits, the new song island becomes the current song Island.
// But having it be its own store lets us ensure that their details are saved
// even if they cancel accidentally or refresh the page or whatever!

import {writable, derived} from 'svelte/store';
import { parse } from 'smarkt';
import { aesthetic } from './new-aesthetic.js';
import { songIsland } from './songIsland.js';

export const newSongIsland = writable({
  song: {
    title: '',
    artist: '',
    src: ''
  },
  'song links': [],
  picture: {
    title: '',
    src: '',
    attribution: '',
    description: ''
  },
  'picture links': [],
  words: '',
  links: []
})

document.addEventListener('DOMContentLoaded', () => {
  fetch('/zine/song-island.zine')
    .then(response => response.text())
    .then(text => parse(text))
    .then(data => newSongIsland.set({
      'picture links': [],
      'song links': [],
      ...data
    }))
})

export const pictureLinks = derived(
  newSongIsland,
  ($nsi, set) => {
    let links = [];
    if ($nsi['picture links']) {
      set($nsi['picture links']);
    } else {
      set(links);
    }
  }
)
export const songLinks = derived(
  newSongIsland,
  ($nsi, set) => {
    let links = [];
    if ($nsi['song links']) {
      set($nsi['song links']);
    } else {
      set(links);
    }
  }
)

export const words = derived(
  newSongIsland,
  ($nsi, set) => {
    let words = '';
    if ($nsi.words) {
      set($nsi.words);
    } else {
      set(words);
    }
  }
)

export const finalized = derived(
  [newSongIsland, aesthetic, songIsland],
  ([$newSongIsland, $aesthetic, $songIsland], set) => {
    set({
      newSongUploaded: $newSongIsland.file !== undefined,
      newPictureUploaded: $aesthetic.pictureFile !== undefined,
      newSongIslandWritten: $newSongIsland !== $songIsland
    })
  }
)
