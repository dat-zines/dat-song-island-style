import { writable, derived } from 'svelte/store';
import { parse } from 'smarkt';
import marked from 'marked';

export const songIsland = writable({
  song: {
    title: '',
    artist: '',
    src: ''
  },
  'song links': [],
  picture: {
    title: '',
    src: '',
    attribution: '',
    description: ''
  },
  'picture links': [],
  words: ''
})

document.addEventListener('DOMContentLoaded', () => {
  fetch('/zine/song-island.zine')
    .then(response => response.text())
    .then(text => {
      return parse(text)
    }) 
    .then(data => songIsland.set({...data}))
})

export const song = derived(
  songIsland,
  ($songIsland, set) => {
    set($songIsland.song.title)
  }
)

export const words = derived(
  songIsland,
  ($songIsland, set) => {
    if (!$songIsland.words) return '<p>...</p>'
    set(marked($songIsland.words));
  }
)

export const pictureLinks = derived(
  songIsland,
  ($songIsland, set) => {
    if ($songIsland['picture links'] == undefined) set([]);
    set($songIsland['picture links']);
  }
)

export const songLinks = derived(
  songIsland,
  ($songIsland, set) => {
    if ($songIsland['song links'] == undefined) set([]);
    set($songIsland['song links']);
  }
)

export const datZine = new DatArchive(window.location);
