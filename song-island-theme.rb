# Hi!  This is the code that made up the song island theme.
# It's written in the sonic pi language.
# You can grab an editor/music-player for sonic pi at
# https://sonic-pi.net

# Then, paste this code into the editor, press run, and
# hear that code play!
# This is all open source and free and such, so cut, copy,
# and modify the song until it sounds sweet to you.

define :triplets do |n1, n2, n3, repeat|
  #takes three notes, and a # of how many times to repeat
  # then plays them notes in order.
  repeat.times do
    play n1, amp: 0.5, pan: -1
    sleep 0.25
    play n2, amp: 0.75, pan: 1
    sleep 0.25
    play n3, pan: 0
    sleep 0.25
  end
end

define :double_note do |n1, n2, repeat|
  # takes two notes, and a repeating number,
  # then plays those two notes in order that many times.
  repeat.times do
    play n1, pan: -1
    sleep 0.25
    play n2
    sleep 0.25
  end
end

# the song
with_fx :echo, phase: 0.5 do
  with_fx :reverb do
    live_loop :song do
      use_synth :saw
      triplets [:c3, :c4], [:d4, :d5], :g4, 4
      double_note :c4, :d4, 2
      triplets :a3, :d4, :g4, 4
      double_note :a3, :d4, 2
      triplets [:g3, :g4], :d4, :g4, 4
      double_note :g3, :d4, 2
      triplets [:f2, :f3], :a3, :c3,  4
      double_note [:f2, :f3], [:c4, :c5],  2
    end
  end
end

# the island.
#It comes as a surprise, and not all the time.
live_loop :island do
  sync :song
  if one_in(2)
    use_synth :hoover
    2.times do
      play [:g2, :g3],sustain: 4
      sleep 4
      play :c3, sustain: 4
      sleep 4
    end
    2.times do
      play [:f2, :f4],sustain: 4
      sleep 4
      play [:c2, :c3], sustain: 4
      sleep 4
    end
  end
end
